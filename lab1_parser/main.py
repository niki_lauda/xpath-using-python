import requests
import urlparse
import datetime
import lxml.etree
from xml.dom.minidom import Document
from urlparse import urljoin
from lxml import etree
from lxml import html

URLS_COUNT = 20
home_url = 'http://xsport.ua/'
home_url2 = 'http://www.meblium.com.ua/documents/52_rus'
scrap_text_query = '//p/text()[string-length(normalize-space(.))>0]|' \
                   '//a/text()[string-length(normalize-space(.))>0]|' \
                   '//span/text()[string-length(normalize-space(.))>0]'


def is_absolute(url):
    return bool(urlparse.urlparse(url).netloc)


def scrap(url, scrap_query):
    page_tree = html.fromstring((requests.get(url)).content)
    return page_tree.xpath(scrap_query)


def links_to_scrap(url):
    link_list = [url, ]
    for rel_link in scrap(url, "//a/@href"):
        abs_link = urljoin(url, rel_link)
        if is_absolute(abs_link) and abs_link.__contains__(url):
            link_list.append(abs_link)
    return link_list


def get_info(good, query):
    try:
        return (good.xpath(query)[0]).encode("UTF-8")
    except IndexError:
        return '<None>'


def scrap_goods(query, s_type):
    ls = []
    for good in scrap(home_url2, query):
        bed = doc.createElement("Bed")
        bed.setAttribute('sales_type', s_type)
        ls.append(bed)
        tags_queries = [('Title', 'child::node()//a[@class="title"]/text()'),
                ('Code', 'child::node()//a[@class="title"]/span/text()'),
                ('Image', 'child::node()//a[@class="illustration_wrapper_inner"]/img/@src'),
                ('Price', 'child::node()//div[@class="information"]/child::node()//p[@class="buy-from"]/text()|'
                          'child::node()//span[@class="value"]/text()')]
        for tag_name, query in tags_queries:
            elem = doc.createElement(tag_name)
            elem.appendChild(doc.createTextNode(get_info(good, query)))
            bed.appendChild(elem)
    return ls


def task1_2():
    global doc
    file_name = 'data1.xml'
    root = etree.Element('data')
    start_time = datetime.datetime.utcnow()
    print "Processing started at [%s]" % start_time
    for link in list(set(links_to_scrap(home_url)))[:URLS_COUNT]:
        page = etree.SubElement(root, 'page', url=link)
        print 'Processing %s' % link
        for img_src in scrap(link, "//img/@src"):
            etree.SubElement(page, 'fragment', type='image').text = img_src
        for text_item in scrap(link, scrap_text_query):
            etree.SubElement(page, 'fragment', type='text').text = text_item
    f = open(file_name, 'wb')
    f.write(etree.tostring(root, encoding='utf-8', pretty_print=True))
    f.close()
    end_time = datetime.datetime.utcnow()
    """TASK 9: show all hyperlinks using XPath"""
    doc = lxml.etree.parse(file_name)
    urls = doc.xpath('//page/@url')
    print 'Processing ended at [%s].\n[%s] web-pages in [%s] processed: %s' \
          % (end_time, URLS_COUNT, end_time - start_time, urls)


def task3_4():
    global doc
    xml_file = 'data2.xml'
    xsl_file = 'store.xsl'
    xhtml_file = 'index.html'
    f = open(xml_file, 'wb')
    doc = Document()
    root_elem = doc.createElement('Furniture')
    doc.appendChild(root_elem)
    sales_attributes = (('good mark-sale ', 'Mark Sale'), ('good mark-leader ', 'Mark Leader'),
                        ('good mark-new ', 'Mark New'), ('good mark-sale new-price', "Mark New Price"))
    start_time = datetime.datetime.utcnow()
    print 'Processing %s started at [%s]' % (home_url2, start_time)
    for sales_type, sales_name in sales_attributes:
        for bed in scrap_goods('//div[@class="%s"]' % sales_type, sales_name):
            root_elem.appendChild(bed)
    end_time = datetime.datetime.utcnow()
    print 'Processing %s ended at [%s]' % (home_url2, end_time)
    print 'Processed in [%s]' % (end_time - start_time)
    f.write(doc.toprettyxml())
    f.close()

    print "Converting XML into XHTML using XSLT..."
    stylesheet = lxml.etree.XSLT(lxml.etree.parse(xsl_file))
    data = lxml.etree.parse(xml_file)
    xhtml = stylesheet(data)
    f1 = open(xhtml_file, 'wb')
    f1.write(etree.tostring(xhtml, encoding='UTF-8', pretty_print=True))
    f1.close()
    print 'Finished'

if __name__ == '__main__':
    task1_2()
    task3_4()

